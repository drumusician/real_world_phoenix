import css from "../css/app.scss"
import "phoenix_html"
import hljs from 'highlight.js/lib/highlight';
import javascript from 'highlight.js/lib/languages/javascript.js';
import elixir from 'highlight.js/lib/languages/elixir.js';
import bash from 'highlight.js/lib/languages/bash.js';
import { Socket } from "phoenix";
import LiveSocket from "phoenix_live_view";

hljs.registerLanguage('javascript', javascript);
hljs.registerLanguage('elixir', elixir);
hljs.registerLanguage('bash', bash);
hljs.initHighlightingOnLoad();

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content");
let liveSocket = new LiveSocket("/live", Socket, { params: { _csrf_token: csrfToken } });
liveSocket.connect()

// Bulma navbar-burger toggle
document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);
        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }
});
