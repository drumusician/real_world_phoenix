defmodule Credo.ReadyForReleaseCheck do
  @moduledoc """
    Checks all lines for a given Regex.

    This is fun!
  """

  @explanation [
    check: @moduledoc,
    params: [
      regex: "All lines matching this Regex will yield an issue."
    ]
  ]
  @default_params [
    # our check will find this line.
    regex: ~r/use Mix.Config/
  ]

  # you can configure the basics of your check via the `use Credo.Check` call
  use Credo.Check, base_priority: :high, category: :custom, exit_status: 0

  @doc false
  def run(%{filename: filename} = source_file, params \\ []) do
    lines = SourceFile.lines(source_file)
    issue_meta = IssueMeta.for(source_file, params)

    case filename do
      "config/releases.exs" ->
        Enum.reduce(lines, [], &process_line(&1, &2, ~r/use Mix.Config/, issue_meta))

      _ ->
        []
    end
  end

  defp process_line({line_no, line}, issues, line_regex, issue_meta) do
    case Regex.run(line_regex, line) do
      nil ->
        issues

      matches ->
        trigger = matches |> List.last()
        new_issue = issue_for(issue_meta, line_no, trigger)
        [new_issue] ++ issues
    end
  end

  defp issue_for(issue_meta, line_no, trigger) do
    # format_issue/2 is a function provided by Credo.Check to help us format the
    # found issue
    format_issue(issue_meta,
      message:
        "You should use `import Config` instead of `use Mix.Config` if you are using releases",
      line_no: line_no,
      trigger: trigger
    )
  end
end
