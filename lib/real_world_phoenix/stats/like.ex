defmodule RealWorldPhoenix.Stats.Like do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  schema "likes" do
    field :slug, :string
    field :user_id, :string
    field :like, :boolean, default: true

    timestamps()
  end

  def changeset(like, attrs \\ %{}) do
    like
    |> cast(attrs, [:slug, :user_id, :like])
  end
end
