defmodule RealWorldPhoenix.Release do
  @moduledoc false
  @app :real_world_phoenix

  def migrate do
    for repo <- repos() do
      Application.ensure_all_started(:ssl)
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    Application.ensure_all_started(:ssl)
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
