defmodule RealWorldPhoenix.Stats do
  @moduledoc false
  import Ecto.Query
  alias RealWorldPhoenix.Stats.Like
  alias RealWorldPhoenix.Repo

  def liked?(slug, user_id) do
    Repo.get_by(Like, slug: slug, user_id: user_id)
  end

  def like_post(attrs) do
    %Like{}
    |> Like.changeset(attrs)
    |> Repo.insert(on_conflict: [set: [like: true]], conflict_target: [:slug, :user_id])
  end

  def dislike_post(attrs) do
    %Like{}
    |> Like.changeset(attrs)
    |> Repo.insert(on_conflict: [set: [like: false]], conflict_target: [:slug, :user_id])
  end

  def get_likes(slug) do
    query = from(l in Like, where: l.slug == ^slug and l.like == ^true)
    Repo.all(query)
  end
end
