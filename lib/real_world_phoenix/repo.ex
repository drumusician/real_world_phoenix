defmodule RealWorldPhoenix.Repo do
  use Ecto.Repo,
    otp_app: :real_world_phoenix,
    adapter: Ecto.Adapters.Postgres
end
