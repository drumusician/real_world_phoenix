defmodule RealWorldPhoenix.Posts do
  @moduledoc false
  alias RealWorldPhoenixWeb.PostView

  def get_all_posts do
    {_, _, templates} = PostView.__templates__()

    templates
    |> map_fields()
    |> reject_future_posts()
    |> Enum.sort_by(& &1.date, {:desc, Date})
  end

  def get_latest_published_post do
    get_all_posts()
    |> List.first()
  end

  defp map_fields(paths) when is_list(paths) do
    paths
    |> Enum.map(&Path.split/1)
    |> Enum.map(&Enum.take(&1, -2))
    |> reject_partials_and_index
    |> Enum.map(&convert_fields/1)
  end

  defp reject_future_posts(list) do
    list
    |> Enum.reject(&(Date.compare(&1.date, Date.utc_today()) == :gt))
  end

  defp reject_partials_and_index(list) do
    list
    |> Enum.reject(fn file -> hd(file) == "index.html" end)
    |> Enum.reject(fn [_date, file] -> String.starts_with?(file, "_") end)
  end

  defp convert_fields([date, file]) do
    %{
      date: get_date(date),
      slug: get_slug(file),
      title: get_human_friendly_title(file)
    }
  end

  defp get_slug(filename) do
    Path.basename(filename, ".html")
  end

  defp get_human_friendly_title(file) do
    get_slug(file)
    |> String.split("_")
    |> Enum.map(&String.capitalize/1)
    |> Enum.join(" ")
  end

  defp get_date(datestring) do
    Date.from_iso8601!(datestring)
  end
end
