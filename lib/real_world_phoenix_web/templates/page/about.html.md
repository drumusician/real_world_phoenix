## Who Am I

Hi! I'm Tjaco. A curious developer / musician from Amsterdam with a passion for Elixir. On this blog I share my thoughts, opinions and ideas about programming, Elixir, Phoenix and anything else related to building applications for web(and sometimes mobile).

## Kabisa

Currently I work as a developer / consultant at [Kabisa](https://kabisa.nl) in the Amsterdam office. Interested in joining us? We are hiring! Send me a message and I'll hook you up with the right people to get you introduced

_Keep Coding!_

