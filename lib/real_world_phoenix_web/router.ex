defmodule RealWorldPhoenixWeb.Router do
  use RealWorldPhoenixWeb, :router
  alias RealWorldPhoenixWeb.Plugs.UserIdCookie

  pipeline :browser do
    plug UserIdCookie
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RealWorldPhoenixWeb do
    pipe_through :browser

    get "/", PostController, :index
    get "/blog/:date/:title", PostController, :show
    get "/about", PageController, :about
    get "/contact", PageController, :contact
  end
end
