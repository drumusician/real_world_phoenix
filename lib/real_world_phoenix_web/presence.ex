defmodule RealWorldPhoenixWeb.Presence do
  @moduledoc false

  use Phoenix.Presence,
    otp_app: :real_world_phoenix,
    pubsub_server: RealWorldPhoenix.PubSub
end
