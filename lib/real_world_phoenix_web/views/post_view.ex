defmodule RealWorldPhoenixWeb.PostView do
  use Phoenix.View, root: "lib/real_world_phoenix_web/templates", pattern: "**/*"
  alias RealWorldPhoenix.Stats

  def get_slug(conn) do
    List.last(conn.path_info)
  end

  def liked?(conn) do
    like = Stats.liked?(get_slug(conn), conn.assigns.user_id)
    if is_nil(like), do: nil, else: like.like
  end
end
