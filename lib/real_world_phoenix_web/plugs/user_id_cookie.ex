defmodule RealWorldPhoenixWeb.Plugs.UserIdCookie do
  @moduledoc false
  import Plug.Conn
  alias Ecto.UUID

  def init(default), do: default

  def call(conn, _default) do
    user_id =
      conn
      |> fetch_session
      |> set_or_get_user_id()

    conn
    |> fetch_session()
    |> put_session(:user_id, user_id)
    |> set_user_id_assigns(user_id)
  end

  def set_or_get_user_id(conn) do
    case get_session(conn, :user_id) do
      nil ->
        UUID.generate()

      user_id ->
        user_id
    end
  end

  defp set_user_id_assigns(conn, user_id) do
    assign(conn, :user_id, user_id)
  end
end
