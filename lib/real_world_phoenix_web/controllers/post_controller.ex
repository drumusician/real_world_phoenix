defmodule RealWorldPhoenixWeb.PostController do
  use RealWorldPhoenixWeb, :controller
  alias RealWorldPhoenix.Posts

  def index(conn, _params) do
    latest_post = Posts.get_latest_published_post()
    redirect(conn, to: "/blog/#{Date.to_string(latest_post.date)}/#{latest_post.slug}")
  end

  def show(conn, %{"title" => title, "date" => date}) do
    render(conn, "blog/#{date}/#{title}.html")
  end
end
