defmodule RealWorldPhoenixWeb.Live.Like do
  @moduledoc false
  use Phoenix.LiveView
  alias RealWorldPhoenix.Stats
  alias RealWorldPhoenixWeb.Endpoint

  def render(assigns) do
    ~L"""
    <div>
    <%= if is_nil(@liked) do %>
        <h4 class="title is-6 has-text-centered"><em>Do you like the interactive features of realworldphoenix.com?</em></h4>
        <span class="icon thumbs is-flex">
          <i class="fas fa-thumbs-up" phx-click="like"></i>
        </span>
        <span class="icon thumbs is-flex">
          <i class="fas fa-thumbs-down" phx-click="dislike"></i>
        </span>
    <% end %>
    <%= if @liked == true do %>
      <h4 class="title is-6 has-text-centered"><strong>You liked this post. Awesome!</strong></h4>
        <span class="icon thumbs is-flex">
          <i class="fas fa-thumbs-up has-text-success" phx-click="dislike"></i>
        </span>
    <% end %>
    <%= if @liked == false do %>
      <h4 class="title is-6 has-text-centered"><strong>Oh no!</strong></h4>
        <span class="icon thumbs is-flex">
          <i class="fas fa-thumbs-down has-text-danger" phx-click="like"></i>
        </span>
    <% end %>
    </div>
    """
  end

  def mount(session, socket) do
    assigns = %{
      liked: session["liked"],
      slug: session["slug"],
      user_id: session["user_id"]
    }

    {:ok, assign(socket, assigns)}
  end

  def handle_event("like", _value, %{assigns: %{slug: slug, user_id: user_id}} = socket) do
    Task.start(fn -> Stats.like_post(%{slug: slug, user_id: user_id}) end)
    Endpoint.broadcast!("blog:#{slug}", "like_added", %{})

    {:noreply, assign(socket, :liked, true)}
  end

  def handle_event("dislike", _value, %{assigns: %{slug: slug, user_id: user_id}} = socket) do
    Task.start(fn -> Stats.dislike_post(%{slug: slug, user_id: user_id}) end)
    Endpoint.broadcast!("blog:#{slug}", "like_removed", %{})

    {:noreply, assign(socket, :liked, false)}
  end
end
