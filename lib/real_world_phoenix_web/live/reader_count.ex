defmodule RealWorldPhoenixWeb.Live.ReaderCount do
  use Phoenix.LiveView
  alias RealWorldPhoenixWeb.Presence

  @moduledoc """
    A small LiveView that shows the number of readers of a post using Phoenix Presence
  """

  def render(assigns) do
    ~L"""
      Readers: <%= @reader_count %>
    """
  end

  def mount(%{slug: slug}, socket) do
    topic = "blog:#{slug}"
    initial_count = Presence.list(topic) |> map_size
    RealWorldPhoenixWeb.Endpoint.subscribe(topic)

    Presence.track(
      self(),
      topic,
      socket.id,
      %{}
    )

    {:ok, assign(socket, :reader_count, initial_count)}
  end

  def handle_info(
        %{event: "presence_diff", payload: %{joins: joins, leaves: leaves}},
        %{assigns: %{reader_count: count}} = socket
      ) do
    reader_count = count + map_size(joins) - map_size(leaves)

    {:noreply, assign(socket, :reader_count, reader_count)}
  end
end
