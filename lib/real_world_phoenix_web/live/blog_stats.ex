defmodule RealWorldPhoenixWeb.Live.BlogStats do
  @moduledoc """
    A LiveVew that gathers stats about posts
  """
  use Phoenix.LiveView
  alias RealWorldPhoenixWeb.Presence
  alias RealWorldPhoenix.Stats

  def render(assigns) do
    ~L"""
      <div class="stats">
        <h3 class="title is-5">Realtime Stats</h3>
        <p>Readers: <%= @reader_count %></p>
        <p>Likes: <%= @likes %></p>
    </div>
    """
  end

  def mount(%{"slug" => slug}, socket) do
    topic = "blog:#{slug}"
    initial_count = Presence.list(topic) |> map_size
    likes = Stats.get_likes(slug) |> length
    RealWorldPhoenixWeb.Endpoint.subscribe(topic)

    Presence.track(
      self(),
      topic,
      socket.id,
      %{}
    )

    {:ok, assign(socket, %{reader_count: initial_count, likes: likes})}
  end

  def handle_info(
        %{event: "presence_diff", payload: %{joins: joins, leaves: leaves}},
        %{assigns: %{reader_count: count}} = socket
      ) do
    reader_count = count + map_size(joins) - map_size(leaves)

    {:noreply, assign(socket, :reader_count, reader_count)}
  end

  def handle_info(%{event: "like_added"}, socket) do
    {:noreply, assign(socket, :likes, socket.assigns.likes + 1)}
  end

  def handle_info(%{event: "like_removed"}, socket) do
    {:noreply, assign(socket, :likes, socket.assigns.likes - 1)}
  end
end
