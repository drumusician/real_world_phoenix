defmodule RealWorldPhoenix.Repo.Migrations.RenameIpToUserId do
  use Ecto.Migration

  def change do
    rename table("likes"), :ip, to: :user_id
  end
end
