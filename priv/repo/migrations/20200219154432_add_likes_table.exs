defmodule RealWorldPhoenix.Repo.Migrations.AddLikesTable do
  use Ecto.Migration

  def change do
    create table("likes") do
      add :slug, :string
      add :ip, :string
      add :like, :boolean, default: true

      timestamps()
    end

    create unique_index(:likes, [:slug, :ip])
  end
end
