use Mix.Config

# Configure your database
config :real_world_phoenix, RealWorldPhoenix.Repo,
  username: "postgres",
  password: "postgres",
  database: "real_world_phoenix_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :real_world_phoenix, RealWorldPhoenixWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
