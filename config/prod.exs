use Mix.Config

config :real_world_phoenix, RealWorldPhoenixWeb.Endpoint,
  url: [
    host: Application.get_env(:real_world_phoenix, :app_hostname),
    port: Application.get_env(:real_world_phoenix, :app_port)
  ],
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true

# Do not print debug messages in production
config :logger, level: :info
