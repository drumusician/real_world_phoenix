# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :real_world_phoenix,
  ecto_repos: [RealWorldPhoenix.Repo]

# Configures the endpoint
config :real_world_phoenix, RealWorldPhoenixWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hxi0TSZF0h1lpmh6jeam08aZ5QwhL7F5FUTdWLUtJd/T+BW93QSiJXCzZiR/QuaB",
  render_errors: [view: RealWorldPhoenixWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RealWorldPhoenix.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "qr2wdeX2Iw0C2FqS8WfTYXrEjDCnUXbi"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :phoenix, :template_engines,
  md: PhoenixMarkdown.Engine,
  scrambled: RealWorldPhoenix.Engines.Scrambled

config :phoenix_markdown,
  earmark: %{
    gfm: true,
    smartypants: false
  },
  server_tags: :all

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
